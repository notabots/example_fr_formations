class RandomFormationBase
{
	name = "French Base Formation";
	class Fixed
	{
		FormationPositionInfo0[] = {0,0,0,0};
	};

	class Pattern
	{
		FormationPositionInfo1[] = {-1,0,0,0};
	};
	
	reorderRules[] = {
		0,2,
		0,3,
		0,4,
		0,5,
		0,6,
		0,7,
		0,8,
		0,9,
		0,1
	};
};

/*
         o O o
*/

class FrenchTrinomeLine: RandomFormationBase
{
	name = "French Trinome Line";
	class Fixed
	{
		FormationPositionInfo0[] = {0, 0, 0, 0, true, "TL"};
		FormationPositionInfo1[] = {0, 5, 0, 0, true, "MG"};
		FormationPositionInfo2[] = {0, -5, 0, 0, true, "GL"};
	};
};

/*
           p
           O
           o
*/

class FrenchTrinomeColumn: RandomFormationBase
{
	name = "French Trinome Column";
	class Fixed
	{
		FormationPositionInfo0[] = {0, 0, 0, 0, true, "TL"};
		FormationPositionInfo1[] = {0, 0, -5, 0, true, "MG"};
		FormationPositionInfo2[] = {0, 0, -10, 0, true, "GL"};
	};
};

/*
           p                       p
           O           O           O
           o           o           o
*/

class FrenchSquadLineTrinomeColumns: RandomFormationBase
{
	name = "French Squad Line Trinome Columns";
	class Fixed
	{
		FormationPositionInfo0[] = {0, 0, 0, 0, true, "SQL"};
		FormationPositionInfo1[] = {0, 30, 0, 0, true, "T1"};
		FormationPositionInfo2[] = {0, -30, 0, 0, true, "T2"};
	};
};

/*
         o O o         O o       o O o
*/

class FrenchSquadLineTrinomeLines: RandomFormationBase
{
	name = "French Squad Line Trinome Lines";
	class Fixed
	{
		FormationPositionInfo0[] = {0, 0, 0, 0, true, "SQL"};
		FormationPositionInfo1[] = {0, 30, 0, 0, true, "T1"};
		FormationPositionInfo2[] = {0, -30, 0, 0, true, "T2"};
	};
};

/*
           p
           O
           o

           O
           o 

           p
           O
           o
*/

class FrenchSquadColumnTrinomeColumns: RandomFormationBase
{
	name = "French Squad Column Trinome Columns";
	class Fixed
	{
		FormationPositionInfo0[] = {0, 0, 0, 0, true, "T1"};
		FormationPositionInfo1[] = {0, 0, -20, 0, true, "SQL"};
		FormationPositionInfo2[] = {0, 0, -40, 0, true, "T2"};
	};
};

/*
         o O o

           O o 

         o O o
*/

class FrenchSquadColumnTrinomeLines: RandomFormationBase
{
	name = "French Squad Column Trinome Lines";
	class Fixed
	{
		FormationPositionInfo0[] = {0, 0, 0, 0, true, "T1"};
		FormationPositionInfo1[] = {0, 0, -20, 0, true, "SQL"};
		FormationPositionInfo2[] = {0, 0, -40, 0, true, "T2"};
	};
};