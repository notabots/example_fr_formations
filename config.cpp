#include "\vbs2\basic_defines.hpp"
#define __CurrentDir__ \vbs2\customer\example_fr_formations

//To pack the addon, put it into the \vbs2\customer folder in your "dev drive" directory (see devRef on packing addons for more details)

//Class necessary for VBS to load the new addon correctly
class CfgPatches
{
	class vbs2_customer_example_fr_formations
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.10;
		requiredAddons[] = {vbs2_editor, vbs2_people, vbs2_vbs_plugins_vbsControl};
		modules[] =
		{
			vbs_core_content_module
		};
	};
};


// formations
class CfgFormations
{
	class West
	{
		#include "cfgFormations.hpp"
	};

	class East : West
	{
		#include "cfgFormations.hpp"
	};

	class Civ : West
	{
		#include "cfgFormations.hpp"
	};

	class Guer : West
	{
		#include "cfgFormations.hpp"
	};
};
